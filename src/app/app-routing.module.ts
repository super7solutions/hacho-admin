import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { FullLayoutComponent } from './layouts/full/full.layout.component';
import { GuardService } from './services/guard.service';
import { SimpleLayoutComponent } from './layouts/simple/simple.layout.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
    },
    {
        path: '',
        component: FullLayoutComponent,
        children: [ 
            {
                path: 'dashboard',
                loadChildren: './pages/dashboard/dashboard.module#DashboardModule'
            },
            {
                path: 'deposit',
                loadChildren: './pages/deposit/deposit.page.module#DepositPageModule'
            },
            {
                path: 'withdraw',
                loadChildren: './pages/withdraw/withdraw.page.module#WithdrawModule'
            },
            {
                path: 'users',
                loadChildren: './pages/user/user.page.module#UserModule'
            },
            {
                path: 'agents',
                loadChildren: './pages/agent/agent.page.module#AgentModule'
            },
            {
                path: 'transactions',
                loadChildren: './pages/transactions/transaction.page.module#TransactionModule'
            },
            {
                path: 'activity',
                loadChildren: './pages/activity/activity.page.module#ActivityModule'
            }

        ],
        canActivate: [GuardService]
    },
    {
        path: '',
        component: SimpleLayoutComponent,
        children: [
            {
                path: '',
                loadChildren: './pages/auth/auth.module#AuthModule'
            }
        ]
    }

];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
