import { User } from "./user.model";

export class DepositTransaction {
    id: number
    amount: number
    userId: number
    user: User
    paymentGateway: string // Which Payment Channel eg. iTrustPay, WeChat
    uuid: string
    depositType: string
    status: string
    transactionFee: number
    successAt: Date
    createdAt: Date
    errorCode: string
}