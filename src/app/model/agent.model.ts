export class Agent {
    id: number
    createdAt: Date
    updatedAt: Date
    name: string
    profit: number
    orderNumber: number
    remark: string
    validBetAmount: string
    rebateRate: number
    memberNumber: number

}