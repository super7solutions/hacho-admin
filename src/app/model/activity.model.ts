import { User } from "./user.model";

export class Activity {
    id: number
    userId: number | string
    action: string
    description: string
    createdAt: Date
    type: string
    user: User
}