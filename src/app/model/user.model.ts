import { LevelUser } from "./level.user.model";

export class User {
    id: number
    levelUserId: number
    levelUser: LevelUser
    username: string
    agency: string
    ip: string
    email: string
    mobile_number: string
    status: string
    credits: number
    password: string
    fundPassword: string
    newPassword: string
    resetFundPassword: string
}