import { User } from "./user.model";

export class Transaction {
    id: number
    type: string
    status: string
    createdAt: Date
    amount: number
    userId: number
    user: User
}