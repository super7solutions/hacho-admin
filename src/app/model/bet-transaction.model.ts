export class BetTransaction {
    id: number
    gameId: number
    gameType: string
    gameCode: string 
    userId: number
    amount: number
    type: string
    createdAt: Date
    status: string
}