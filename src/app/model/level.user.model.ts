export class LevelUser {
    id: number
    name: string
    depositTotalAmount: number
    totalBetAmount: number
    remarks: string
    createdAt: Date
    updatedAt: Date
    status: string
}