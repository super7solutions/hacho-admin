import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListUserPageComponent } from './list/list.user.page.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ClarityModule, ClrFormsModule, ClrFormsNextModule, ClrDatagridModule, ClrTabsModule, ClrDropdownModule, ClrModalModule } from '@clr/angular';
import { UserRoutingModule } from './user-page-routing.module';
import { ViewUserPageComponent } from './view/view.user.page.component';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { OrderModule } from 'ngx-order-pipe';
import { ListLevelUserPageComponent } from './level/list/list.level.user.page.component';

@NgModule({
    declarations: [
        ListUserPageComponent,
        ViewUserPageComponent,
        ListLevelUserPageComponent
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        ClrFormsNextModule,
        ClrDatagridModule,
        FormsModule,
        ClarityModule,
        ClrFormsModule,
        ClrTabsModule,
        ClrDropdownModule,
        ClrModalModule,
        UserRoutingModule,
        TranslateModule,
        SharedModule,
        NgxChartsModule,
        OrderModule
    ],
    exports: [],
    providers: [],
})
export class UserModule {}