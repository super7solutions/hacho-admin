import { Component, OnInit } from '@angular/core'
import { UserService } from '../../../services/user.service'
import { Observable } from 'rxjs'
import { User } from '../../../model/user.model'
import { ClrDatagridStateInterface } from '@clr/angular'

@Component({
    selector: 'app-user-list',
    templateUrl: './list.user.page.component.html',
    styleUrls: ['./list.user.page.component.scss']
})
export class ListUserPageComponent implements OnInit {
    public users: User []
    public usersLoading:  boolean = true
    constructor(
        private userService: UserService
    ) {
        this.userService.subscribeToUsers().subscribe(users => {
            this.usersLoading = false;
            this.users = users;
        })
    }

    ngOnInit(): void {
        this.userService.fetchUsers()
    }

    refresh(state: ClrDatagridStateInterface) {
        console.log('stam',state)
        this.userService.fetchUsers(state)
    }
}
