import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { ListUserPageComponent } from './list/list.user.page.component';
import { ViewUserPageComponent } from './view/view.user.page.component';
import { ListLevelUserPageComponent } from './level/list/list.level.user.page.component';

const routes: Routes = [
    { path: '', component: ListUserPageComponent },
    { path: ':user', component: ViewUserPageComponent },
    { path: 'level/list', component: ListLevelUserPageComponent}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UserRoutingModule {}
