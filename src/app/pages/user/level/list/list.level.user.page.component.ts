import { Component, OnInit } from '@angular/core';
import { LevelUser } from 'src/app/model/level.user.model';
import { UserService } from 'src/app/services/user.service';
import { ClrDatagridStateInterface } from '@clr/angular';

@Component({
    selector: 'app-list-level-user-page',
    templateUrl: './list.level.user.page.component.html',
    styleUrls: ['./list.level.user.page.component.scss']
})
export class ListLevelUserPageComponent implements OnInit {
    public levelUsers: LevelUser []
    public levelUsersLoading: boolean = true
    public displayModal: boolean = false
    constructor(
        private userService: UserService
    ) {
        this.userService.subscribeToLevelUserList()
        .subscribe(levelUsers => {
            this.levelUsersLoading = false
            this.levelUsers = levelUsers
        })
    }

    ngOnInit(): void { }

    fetchLevelUser(state: ClrDatagridStateInterface = null) {
        this.userService.fetchLevelUser(state)
    }

    setDisplayModal(show: boolean = false) {
        this.displayModal = show
    }
}
