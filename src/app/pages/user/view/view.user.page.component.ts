import { Component, OnInit, OnDestroy } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { UserService } from '../../../services/user.service'
import { User } from '../../..//model/user.model'
import { TransactionService } from '../../../services/transaction.service'
import { Observable } from 'rxjs'
import { BetTransaction } from '../../../model/bet-transaction.model'
import { Transaction } from '../../../model/transaction.model'
import { ClrDatagridStateInterface } from '@clr/angular'
import * as shape from 'd3-shape'
import { Activity } from '../../../model/activity.model';
import { ActivityService } from 'src/app/services/activity.service';
@Component({
    selector: 'app-view-user-page',
    templateUrl: './view.user.page.component.html',
    styleUrls: ['./view.user.page.component.scss']
})
export class ViewUserPageComponent implements OnInit, OnDestroy {
    private userId : number
    public user : User

    public displayModal : boolean = false
    public displayModalResetPassword : boolean = false
    public displayModalResetFundPassword : boolean = false
    public activities: Activity[];

    public bets: BetTransaction []
    public betLoading: boolean = true

    public transactions: Transaction [] 
    public transactionLoading: boolean = true 

    public single: any[]
    public view: any[] = [550, 350]
    public showXAxis = true
    public roundDomains = true
    public showYAxis = true
    public gradient = false
    public showLegend = false
    public showXAxisLabel = true
    public xAxisLabel = 'Date'
    public showYAxisLabel = true
    public yAxisLabel = 'Amount'
    public timeline = true
    public curve = shape.curveCardinal
    public colorScheme = {
      domain: [ '#A10A28' ]
    }
    public autoScale = true
    public showGraph = false;
    constructor(
        private route: ActivatedRoute,
        private userService: UserService,
        private transactionService: TransactionService,
        private activityService: ActivityService
    ) {
        this.single = new Array
        this.single  = [
            { name: '', series: [ ] }
        ]
        this.transactionService.subscribeToTransactionList().subscribe(transactions => {
            this.transactions = transactions
            this.transactionLoading = false
        })
        // this.transactionLoading = this.transactionService.subscribeToTransactionLoading()
        this.transactionService.subscribeToBetList().subscribe(bets => {
            this.bets = bets
        })
        this.userService.subscribeToActivities().subscribe(activities => {
            this.groupActivitiesByDate(activities);
        })
        this.transactionService.subscribeToBetLoading().subscribe(loading => {
            this.betLoading = loading;

            if(loading == false) {
                this.fetchGraphData();
            }
        })
    }

    ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.userId = params['user'] ? params['user'] : null
            if(this.userId) {
                this.getUser()
                this.fetchBets()
                this.fetchTransactions()
                this.userService.fetchActivities(this.userId)
            }


            this.postActivity(`view user ${this.userId}`, 'navigate', 'view')
        })

    }

    ngOnDestroy() {
        this.transactionService.clearData()
        this.userService.clearActivities()
        this.clearGraphData()
    }

    fetchBets(state: ClrDatagridStateInterface = null) {
        this.transactionService.fetchBetList(this.userId, state)
    }

    fetchTransactions(state: ClrDatagridStateInterface = null) {
        this.transactionService.fetchTransactionList(this.userId, state)
    }

    getUser() {
        this.userService.getUserById(this.userId).subscribe(user => {
            this.single[0].name = user.username;
            this.user = user
        })
    }

    clearGraphData() {
        this.showGraph = false
        this.single = [
            { name: '', series: [] }
        ]
    }
    
    fetchGraphData() {
        this.bets.forEach(bet => {
            let newDate = formatDate(bet.createdAt)

            let index = this.single[0].series.findIndex(object => newDate == object.name);
            if(index >= 0) {
                this.single[0].series[index].value += bet.amount
            } else {
                this.single[0].series.push({
                    name: newDate,
                    value: bet.amount
                })
            }
        })

        this.showGraph = true;
    }

    setModalDisplay (display: boolean = false) {
        this.displayModal = display
    }
    
    setdisplayModalResetPassword(display: boolean = false) {
        this.displayModalResetPassword = display
    }

    setDisplayModalResetFundPassword(display: boolean = false) {
        this.displayModalResetFundPassword = display
    }

    confirmResetPassword() {
        let user = this.user
        this.userService.updateUser(user).subscribe(result => {
            this.getUser()
            this.setdisplayModalResetPassword()
            this.setDisplayModalResetFundPassword()
        })


        this.postActivity(`reset password user ${this.userId}`, 'update', 'update')
    }

    changeAccountStatus() {
        let user = this.user
        user.status = user.status == 'active' ? 'lock' : 'active'
        this.userService.updateUser(user).subscribe(() => {
            this.getUser()
            this.setModalDisplay()
        })


        this.postActivity(`${user.status} user ${this.userId}`, 'update', 'update')
    }

    postActivity(
        description: string,
        action: string,
        type: string
    ) {
        this.activityService.postActivty(
            description,
            action,
            type
        )
    }
    private groupActivitiesByDate(activities) {
        let storage = [];
        activities.forEach(activity => {
            let newDate = formatDate(activity.createdAt);
            let index = storage.findIndex(item => item['date'] == newDate)
            if(index >= 0) {
                storage[index]['value'].push(activity);
            } else {
                storage.push({
                    date: newDate,
                    value: [
                        activity
                    ] 
                })
            }
        });
        this.activities = storage;
    }
}
function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear()

    if (month.length < 2) month = '0' + month
    if (day.length < 2) day = '0' + day

    return [year, month, day].join('-')
}
