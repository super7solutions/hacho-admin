import { Component, OnInit } from '@angular/core';
import { Transaction } from '../../../model/transaction.model';
import { ClrDatagridStateInterface } from '@clr/angular';
import { TransactionService } from '../../../services/transaction.service';

@Component({
    selector: 'app-list-transaction-page',
    templateUrl: './list.transaction.page.component.html',
    styleUrls: ['./list.transaction.page.component.scss']
})
export class ListTransactionPageComponent implements OnInit {
    public transactions: Transaction[] = []
    public transactionLoading: boolean 
    constructor(
        private transactionService: TransactionService
    ) {
        this.transactionService.subscribeToTransactionList()
        .subscribe(transactions => {
            this.transactions = transactions
        })

        this.transactionService.subscribeToTransactionLoading()
        .subscribe(loading => {
            this.transactionLoading = loading
        })
    }

    ngOnInit(): void {
        this.fetchTransactions()
    }

    fetchTransactions(state: ClrDatagridStateInterface = null) {
        this.transactionService.fetchTransactionList(null, state)
    }
}
