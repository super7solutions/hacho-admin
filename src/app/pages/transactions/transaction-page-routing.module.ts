import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { ListTransactionPageComponent } from './list/list.transaction.page.component';


const routes: Routes = [
    {
        path: 'list',
        component: ListTransactionPageComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TransactionRoutingModule {}
