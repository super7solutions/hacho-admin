import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransactionRoutingModule } from './transaction-page-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ClrDatagridModule, ClrFormsNextModule } from '@clr/angular';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ListTransactionPageComponent } from './list/list.transaction.page.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
    declarations: [
        ListTransactionPageComponent
    ],
    imports: [
        CommonModule,
        TransactionRoutingModule,
        SharedModule,
        ClrDatagridModule,
        ClrFormsNextModule,
        ClrFormsNextModule,
        ReactiveFormsModule,
        FormsModule,
        TranslateModule
    ],
    exports: [],
    providers: [],
})
export class TransactionModule {}