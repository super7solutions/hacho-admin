import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { ListWithdrawComponent } from './list/list.withdraw.page.component';

const routes: Routes = [
    { path: 'list', component: ListWithdrawComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class WithdrawRoutingModule {}
