import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { WithdrawRoutingModule } from './withdraw-page-routing.module';
import { ClrDatagridModule, ClrFormsNextModule, ClrModalModule, ClrIconModule } from '@clr/angular';
import { SharedModule } from '../../shared/shared.module';
import { ListWithdrawComponent } from './list/list.withdraw.page.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
    declarations: [
        ListWithdrawComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        WithdrawRoutingModule,
        ClrDatagridModule,
        ClrFormsNextModule,
        ClrModalModule,
        ClrIconModule,
        SharedModule,
        TranslateModule
    ],
    exports: [],
    providers: [],
})
export class WithdrawModule {}