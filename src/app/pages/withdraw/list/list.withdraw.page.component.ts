import { Component, OnInit, OnDestroy } from '@angular/core';
import { TransactionService } from '../../../services/transaction.service';
import { WithdrawTransaction } from '../../../model/withdraw-transaction.model';
import { ClrDatagridStateInterface } from '@clr/angular';

@Component({
    selector: 'app-withdraw-list',
    templateUrl: './list.withdraw.page.component.html',
    styleUrls: ['./list.withdraw.page.component.scss']
})
export class ListWithdrawComponent implements OnInit,OnDestroy {
    public withdrawTransactions: WithdrawTransaction[]
    public withdraw: WithdrawTransaction
    public displayModal: boolean = false

    constructor(
        private transactionService: TransactionService
    ) {
        this.transactionService.subscribeToWithdrawTransactionList()
        .subscribe(withdrawList => {
            this.withdrawTransactions = withdrawList
        })
    }

    ngOnInit(): void {
        this.fetchWithdrawList()
    }

    ngOnDestroy():void {
        this.transactionService.clearWithdrawList()
    }

    fetchWithdrawList(state: ClrDatagridStateInterface = null) {
        this.transactionService.fetchWithdrawList(state)
    } 

    viewWithdraw(withdraw: WithdrawTransaction) {
        console.log(withdraw)
        this.withdraw = withdraw
        this.setDisplayModal(true)
    }

    setDisplayModal(display: boolean = false) {
        this.displayModal = display
    }
}
