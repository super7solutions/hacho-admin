import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { ActivityService } from 'src/app/services/activity.service';

@Component({
    selector: 'app-logout',
    template: ''
})
export class LogoutComponent implements OnInit {
    constructor(
        private authService: AuthService,
        private router: Router,
        private activityService: ActivityService
    ) { }

    ngOnInit(): void {
        this.activityService.postActivty(
            'logout user',
            'logout',
            'logout'
        )

        this.logout()
    }

    logout() {
        this.authService.logout();
        this.router.navigateByUrl('/login');
    }
}
