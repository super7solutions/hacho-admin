import { Component, OnInit } from '@angular/core'
import { AuthService } from '../../../services/auth.service'
import { Router } from '@angular/router'

@Component({
    selector: 'app-login',
    templateUrl: './login.page.component.html',
    styleUrls: ['./login.page.component.scss']
})
export class LoginPageComponent implements OnInit {
    public form : object = null
    constructor(
        private authService: AuthService,
        private router: Router
    ) {
        this.authService.subscribeToIsAuthenticated()
        .subscribe(result => {
            if(result == true) {
                this.router.navigateByUrl('/')
            }
        })
    }

    ngOnInit(): void {
        this.form = {
            username: '',
            password: '',
            rememberMe: false
        }
    }

    login(e) {
        e.preventDefault()
        this.authService.login(this.form['username'], this.form['password'])
    }
}
