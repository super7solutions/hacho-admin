import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginPageComponent } from './login/login.page.component';
import { AuthRoutingModule } from './auth-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ClarityModule, ClrFormsModule, ClrFormsNextModule } from '@clr/angular';
import { LogoutComponent } from './logout/logout.component';

@NgModule({
    declarations: [
        LoginPageComponent,
        LogoutComponent
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        ClrFormsNextModule,
        FormsModule,
        ClarityModule,
        ClrFormsModule,
        AuthRoutingModule
    ],
    exports: [],
    providers: [],
})
export class AuthModule {}