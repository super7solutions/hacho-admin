

import { Component, OnInit } from '@angular/core';
import { ActivityService } from 'src/app/services/activity.service';
import { AuthService } from 'src/app/services/auth.service';
@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.page.component.html',
    styleUrls: ['./dashboard.page.component.scss']
})
export class DashboardComponent implements OnInit {
    constructor(
        private activityService: ActivityService,
    ) { }
    ngOnInit(): void {
        this.activityService.postActivty(
            'visit dashboard',
            'navigate',
            ''
        )
    }
}