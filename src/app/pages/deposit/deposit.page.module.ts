import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListDepositPageComponent } from './list/list.deposit.page.component';
import { DepositPageRoutingModule }  from './deposit-page-routing.module';
import { TranslateModule } from '@ngx-translate/core';
import { ClarityModule, ClrButtonGroupModule, ClrDropdownModule, ClrButtonModule } from '@clr/angular';
import { SharedModule } from 'src/app/shared/shared.module';
@NgModule({
    declarations: [
        ListDepositPageComponent
    ],
    imports: [ 
        CommonModule,
        DepositPageRoutingModule,
        TranslateModule,
        ClarityModule,
        SharedModule,
        ClrDropdownModule,
        ClrButtonModule
     ],
    exports: [],
    providers: [],
})
export class DepositPageModule {}