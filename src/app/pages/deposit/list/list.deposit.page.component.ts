import { Component, OnInit, OnDestroy } from '@angular/core';
import { DepositTransaction } from 'src/app/model/deposit-transaction.model';
import { TransactionService } from 'src/app/services/transaction.service';
import { ClrDatagridStateInterface } from '@clr/angular';
import { ActivityService } from 'src/app/services/activity.service';

@Component({
    selector: 'app-list-deposit-page',
    templateUrl: './list.deposit.page.component.html',
    styleUrls: ['./list.deposit.page.component.scss']
})
export class ListDepositPageComponent implements OnInit, OnDestroy {   
    public displayModal: boolean = false
    public displayAuditModal: boolean = false
    public deposit: DepositTransaction
    public depositTransactions: DepositTransaction []
    constructor(
        private transactionService: TransactionService,
        private activityService: ActivityService
    ) { 
        this.transactionService.subscribeToDepositTransactionList()
        .subscribe(depositTransactions => {
            this.depositTransactions = depositTransactions
        })
    }

    ngOnInit(): void {
        this.initilizeDepositTransactions()
    }

    ngOnDestroy(): void {
        this.destroyDepositTransactions()
    }
    
    refresh(state: ClrDatagridStateInterface) {
        this.transactionService.fetchDepositList(state)
    }
    private initilizeDepositTransactions () {
        this.transactionService.fetchDepositList()
    } 
    
    private destroyDepositTransactions () {
        this.transactionService.clearDepositList()
    } 

    openModal(type: string,deposit : DepositTransaction) {
        this.deposit = deposit
        if(type == 'view') {
            this.displayModal = true
            this.postActivity(
                `view deposit id: ${deposit.id}`,
                'view',
                'view'
            )
        } else {
            this.displayAuditModal = true
            this.postActivity(
                `open audit deposit id: ${deposit.id}`,
                'open',
                'open'
            )
        }
    }

    closeModal(type: string) {
        console.log(type)
        if(type == 'view') {
            this.displayModal = false
        } else {
            this.displayAuditModal = false
        }
    }

    saveAudit(deposit: DepositTransaction) {
        this.transactionService.saveAuditChanges(deposit).subscribe(result => {
            console.log('update result', result)
            this.initilizeDepositTransactions()
            this.postActivity(
                `update audit deposit id: ${deposit.id} status: ${deposit.status}`,
                'update',
                'success'
            )
        })
    }

    postActivity(
        description: string,
        action: string,
        type: string
    ) {
        this.activityService.postActivty(
            description,
            action,
            type
        )
    }
}
