import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { ListDepositPageComponent } from './list/list.deposit.page.component';
const routes: Routes = [
 
    {
        path: 'list',
        component: ListDepositPageComponent
    },
  

]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DepositPageRoutingModule {}
