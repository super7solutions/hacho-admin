import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { ListAgentPageComponent } from './list/list.agent.page.component';

const routes: Routes = [
    {
        path: 'list',
        component: ListAgentPageComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AgentPageRoutingModule {}
