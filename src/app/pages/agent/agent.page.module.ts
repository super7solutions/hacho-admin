import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ClrDatagridModule, ClrFormsNextModule } from '@clr/angular';
import { SharedModule } from 'src/app/shared/shared.module';
import { ListAgentPageComponent } from './list/list.agent.page.component';
import { AgentPageRoutingModule } from './agent-page-routing.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
    declarations: [
        ListAgentPageComponent
    ],
    imports: [
        CommonModule,
        AgentPageRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        ClrDatagridModule,
        ClrFormsNextModule,
        SharedModule,
        TranslateModule
    ],
    exports: [],
    providers: [],
})
export class AgentModule {}