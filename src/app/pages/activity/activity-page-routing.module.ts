import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { ListActivityPageComponent } from './list/list.activity.page.component';

const routes: Routes = [
    { path: '', component: ListActivityPageComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ActivityPageRoutingModule {}
