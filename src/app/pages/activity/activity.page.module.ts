import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ClrDatagridModule, ClrFormsNextModule } from '@clr/angular';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { ListActivityPageComponent } from './list/list.activity.page.component';
import { ActivityPageRoutingModule } from './activity-page-routing.module'
@NgModule({
    declarations: [
        ListActivityPageComponent
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        ClrDatagridModule,
        ClrFormsNextModule,
        TranslateModule,
        SharedModule,
        ActivityPageRoutingModule
    ],
    exports: [],
    providers: [],
})
export class ActivityModule {}