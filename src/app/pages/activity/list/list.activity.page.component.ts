import { Component, OnInit } from '@angular/core';
import { ClrDatagridStateInterface } from '@clr/angular';
import { ActivityService } from '../../../services/activity.service';
import { Activity } from '../../../model/activity.model';

@Component({
    selector: 'app-list-activity-page',
    templateUrl: './list.activity.page.component.html',
    styleUrls: ['./list.activity.page.component.scss']
})
export class ListActivityPageComponent implements OnInit {
    activities : Activity []
    activityLoading: boolean = true
    constructor(
        private activityService: ActivityService
    ) {
        this.activityService.subscribeToActivities()
        .subscribe(activities => {
            this.activities = activities
            this.activityLoading = false
        })
    }

    ngOnInit(): void {
        this.fetchActivities()
    }
    

    fetchActivities(state: ClrDatagridStateInterface = null) {
        this.activityService.fetchActivities(state)
    }
}
