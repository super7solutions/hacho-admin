import { Injectable } from '@angular/core'
import { BaseService } from './base.service'
import { HttpClient } from '@angular/common/http'
import { SessionService } from './session.service'
import { BehaviorSubject, Observable } from 'rxjs'
import { map } from 'rxjs/operators'
import { BetTransaction } from '../model/bet-transaction.model'
import { Transaction } from '../model/transaction.model'
import { WithdrawTransaction } from '../model/withdraw-transaction.model';
import { DepositTransaction } from '../model/deposit-transaction.model';
import { User } from '../model/user.model';
import { ClrDatagridStateInterface } from '@clr/angular';

@Injectable({
    providedIn: 'root'
})
export class TransactionService extends BaseService {
    private betList: BehaviorSubject < BetTransaction [] >
    private transactionList: BehaviorSubject < Transaction [] >
    private withdrawTransactionList: BehaviorSubject < WithdrawTransaction [] >
    private depositTransactionList: BehaviorSubject < DepositTransaction [] >
    private betLoading: BehaviorSubject < boolean >
    private transactionLoading: BehaviorSubject < boolean >
    constructor(
        http: HttpClient,
        sessionService: SessionService
    ) {
       super(http,sessionService) 
       this.betList = < BehaviorSubject < BetTransaction[] > > new BehaviorSubject([])
       this.transactionList = < BehaviorSubject < Transaction [] > > new BehaviorSubject([])
       this.withdrawTransactionList = < BehaviorSubject < WithdrawTransaction [ ] > > new BehaviorSubject([])
       this.depositTransactionList = < BehaviorSubject < DepositTransaction [ ] > > new BehaviorSubject([]);
       this.betLoading = < BehaviorSubject < boolean > > new BehaviorSubject(null)
       this.transactionLoading = < BehaviorSubject < boolean > > new BehaviorSubject(null)
    }

    fetchTransactionList(userId: number = null, state: ClrDatagridStateInterface = null) {
        this.setTransactionLoading()
        let url = userId ? this.buildTransactionUrl(userId) : '/transactions?_expand=user'
        this.$localGet(url, state)
        .pipe(map(mapToTransactions))
        .subscribe(transactions => {
            this.transactionList.next(transactions)
            this.setTransactionLoading(false)
        })

    }

    fetchBetList(userId: number = null, state: ClrDatagridStateInterface = null) {
        this.setBetLoading()
        this.$localGet(this.buildBetUrl(userId), state)
        .pipe(map(mapToBets))
        .subscribe(bets => {
            this.betList.next(bets)
            this.setBetLoading(false)
        })
    }

    fetchDepositList(state: ClrDatagridStateInterface = null) {
        this.$localGet('/deposit?_expand=user', state)
        .pipe(map(mapToDepositTransactions))
        .subscribe(depositTransactions => {
            this.depositTransactionList.next(depositTransactions)
        })
    }

    fetchWithdrawList(state: ClrDatagridStateInterface = null) {
        this.$localGet('/withdraw?_expand=user', state)
        .pipe(map(mapToWithdrawTransactions))
        .subscribe(withdrawTransactions => {
            this.withdrawTransactionList.next(withdrawTransactions)
        })
    }

    subscribeToTransactionList(): Observable < Transaction []> {
        return this.transactionList.asObservable()
    }

    subscribeToBetList(): Observable < BetTransaction [] > {
        return this.betList.asObservable()
    }

    subscribeToWithdrawTransactionList(): Observable < WithdrawTransaction []> {
        return this.withdrawTransactionList.asObservable()
    }

    subscribeToDepositTransactionList(): Observable < DepositTransaction [] > {
        return this.depositTransactionList.asObservable()
    }

    subscribeToTransactionLoading() : Observable < boolean > {
        return this.transactionLoading.asObservable()
    }

    subscribeToBetLoading() : Observable < boolean > {
        return this.betLoading.asObservable()
    }
    
    private setBetLoading(loading: boolean = true) {
        setTimeout(() => {
            this.betLoading.next(loading)
        }, 50)
    }

    private setTransactionLoading(loading: boolean = true) {
        setTimeout(() => {
            this.transactionLoading.next(loading)
        }, 50)
    }

    private buildTransactionUrl(
        userId: number = null
        ) {
        let url = `/transactions?type_ne=BET&_expand=user`
        if(userId != null) {
            url += `&userId=${userId}`
        }
        return url
    }

    private buildBetUrl(
        userId : number = null 
    ) {
        let url = `/transactions?type=BET&_expand=user`
        if(userId != null) {
            url += `&userId=${userId}`
        }
        url += '&_expand=gameProvider'

        return url
    }

    clearData() {
        // this.transactionLoading.next(null)
        this.betLoading.next(null)
        this.transactionList.next([])
        this.betList.next([])
    }

    clearDepositList() {
        this.depositTransactionList.next([])
    }

    clearWithdrawList() {
        this.withdrawTransactionList.next([])
    }

    saveAuditChanges(deposit: DepositTransaction) {
        delete deposit.user
        return this.$localPut(`/deposit/${deposit.id}`, deposit)
    }
}

function mapToTransactions(transactions):Transaction [] {
    return transactions.map(mapToTransaction)
}

function mapToTransaction(transaction): Transaction {
    let newTransaction = new Transaction
    newTransaction.id = transaction['id']
    newTransaction.amount = transaction['amount']
    newTransaction.type = transaction['type']
    newTransaction.userId = transaction['userId']
    newTransaction.createdAt = transaction['createdAt']
    newTransaction.status = transaction['status']
    newTransaction.user = mapToUser(transaction['user'])
    return newTransaction
}

function mapToBets(bets):BetTransaction [] {
    return bets.map(mapToBet)
}

function mapToBet(bet): BetTransaction {
    let newBet = new BetTransaction
    newBet.id = bet['id']
    newBet.gameId = bet['gameProviderId']
    newBet.gameType = bet['gameProvider']['type']
    newBet.gameCode = bet['gameProvider']['code']
    newBet.amount = bet['amount']
    newBet.status = bet['status']
    newBet.type = bet['type']
    newBet.userId = bet['userId']
    newBet.createdAt = bet['createdAt']

    return newBet
}

function mapToWithdrawTransactions(withdrawTransactions): WithdrawTransaction [] {
    return withdrawTransactions.map(mapToWithdrawTransaction)
}

function mapToWithdrawTransaction(withdrawTransaction): WithdrawTransaction {
    let newWithdrawTransaction = new WithdrawTransaction
    newWithdrawTransaction.id = withdrawTransaction['id']
    newWithdrawTransaction.amount = withdrawTransaction['amount']
    newWithdrawTransaction.userId = withdrawTransaction['userId']
    newWithdrawTransaction.user = mapToUser(withdrawTransaction['user'])
    newWithdrawTransaction.paymentGateway = withdrawTransaction['paymentGateway']
    newWithdrawTransaction.uuid = withdrawTransaction['uuid']
    newWithdrawTransaction.depositType = withdrawTransaction['depositType']
    newWithdrawTransaction.status = withdrawTransaction['status']
    newWithdrawTransaction.transactionFee = withdrawTransaction['transactionFee']
    newWithdrawTransaction.successAt = withdrawTransaction['successAt']
    newWithdrawTransaction.createdAt = withdrawTransaction['createdAt']
    newWithdrawTransaction.errorCode = withdrawTransaction['errorCode']

    return newWithdrawTransaction
}

function mapToDepositTransactions(depositTransactions): DepositTransaction [] {
    return depositTransactions.map(mapToDepositTransaction)
}

function mapToDepositTransaction(depositTransaction): DepositTransaction {
    let newDepositTransaction = new DepositTransaction
    newDepositTransaction.id = depositTransaction['id']
    newDepositTransaction.amount = depositTransaction['amount']
    newDepositTransaction.userId = depositTransaction['userId']
    newDepositTransaction.user = mapToUser(depositTransaction['user'])
    newDepositTransaction.paymentGateway = depositTransaction['paymentGateway']
    newDepositTransaction.uuid = depositTransaction['uuid']
    newDepositTransaction.depositType = depositTransaction['depositType']
    newDepositTransaction.status = depositTransaction['status']
    newDepositTransaction.transactionFee = depositTransaction['transactionFee']
    newDepositTransaction.successAt = depositTransaction['successAt']
    newDepositTransaction.createdAt = depositTransaction['createdAt']
    newDepositTransaction.errorCode = depositTransaction['errorCode']

    return newDepositTransaction
}

function mapToUser(user): User {
    let newUser = new User;
    newUser.id = user['id'];
    newUser.username = user['username'];
    newUser.email = user['email'];
    newUser.agency = user['agency']
    newUser.ip = user['ip']
    newUser.mobile_number = user['mobile_number'];
    newUser.status = user['status'];
    newUser.credits = user['credits'];

    return newUser;
}
