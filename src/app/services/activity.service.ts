import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';
import { SessionService } from './session.service';
import { Activity } from '../model/activity.model';
import { BehaviorSubject, Observable } from 'rxjs';
import { ClrDatagridStateInterface } from '@clr/angular';
import { map } from 'rxjs/operators';
import { User } from '../model/user.model';

@Injectable({
    providedIn: 'root'
})
export class ActivityService extends BaseService {
    private activities: BehaviorSubject< Activity [ ] >
    constructor(
        http: HttpClient,
        sessionService: SessionService
    ) {
        super(http, sessionService)
        this.activities = < BehaviorSubject < Activity [ ] > > new BehaviorSubject([])
    }

    postActivty(
        description: string,
        action: string,
        type: string
    ) {
        let activity = new Activity
        activity.userId = this.sessionService.getSessionUserId()
        activity.description = description
        activity.action = action
        activity.type = type
        activity.createdAt = new Date()

        console.log('post activity', activity)
        this.$localPost('/activities', activity)
        .subscribe(result => {
            console.log('post result',result)
        })
    }

    fetchActivities(state: ClrDatagridStateInterface = null) {
        this.$localGet('/activities?_expand=user', state)
        .pipe(map(mapToActivities))
        .subscribe(activities => {
            this.activities.next(activities)
        })
    }

    subscribeToActivities(): Observable < Activity [ ] > {
        return this.activities.asObservable()
    }
}

function mapToActivities(activities: Activity []): Activity [] {
    return activities.map(mapToActivity)
}

function mapToActivity(activity: Activity): Activity {
    let activityObject = new Activity
    activityObject.id = activity['id']
    activityObject.userId = activity['userId']
    activityObject.action = activity['action']
    activityObject.user = mapToUser(activity['user'])
    activityObject.description = activity['description']
    activityObject.type = activity['type']
    activityObject.createdAt = activity['createdAt']
    
    return activityObject
}

function mapToUser(user: User): User {
    let newUser = new User
    newUser.id = user['id']
    newUser.username = user['username']
    newUser.email = user['email']
    newUser.agency = user['agency']
    newUser.mobile_number = user['mobile_number']
    newUser.status = user['status']
    newUser.credits = user['credits']
    return newUser
}