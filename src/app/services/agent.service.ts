import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SessionService } from './session.service';
import { BaseService } from './base.service';
import { BehaviorSubject } from 'rxjs';
import { Agent } from '../model/agent.model';
import { ClrDatagridStateInterface } from '@clr/angular';

@Injectable()
export class AgentService extends BaseService{
    private agents: BehaviorSubject < Agent [] >
    constructor(
        http: HttpClient,
        sessionService: SessionService
    ) {
        super(http, sessionService)

        this.agents = < BehaviorSubject < Agent [] > > new BehaviorSubject(null)
    }

    fetchAgents(state: ClrDatagridStateInterface = null) {

    }

    clearAgentList() {
        this.agents.next([])
    }
}

function mapToAgents(agents) : Agent [] {
    return agents.map(mapToAgent)
}

function mapToAgent(agent): Agent {
    let newAgent = new Agent

    newAgent.id = agent['id']
    newAgent.createdAt = agent['createdAt']
    newAgent.memberNumber = agent['memberNumber']
    newAgent.name = agent['name']
    newAgent.profit = agent['profit']
    newAgent.orderNumber = agent['orderNumber']
    newAgent.rebateRate = agent['rebateRate']
    newAgent.remark = agent['remark']
    newAgent.updatedAt = agent['updatedAt']
    newAgent.validBetAmount = agent['validBetAmount']
    
    return newAgent
}