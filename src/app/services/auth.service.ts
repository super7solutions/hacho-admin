import { Injectable } from '@angular/core'
import { BehaviorSubject, Observable } from 'rxjs'
import { BaseService } from './base.service'
import { HttpClient } from '@angular/common/http'
import { SessionService } from './session.service'
import { User } from '../model/user.model'
import { map } from 'rxjs/operators'

@Injectable({
    providedIn: 'root'
})
export class AuthService extends BaseService{
    private isAuthenticated: BehaviorSubject< boolean >
    private currentUser: BehaviorSubject< User >
    constructor(
        http: HttpClient,
        sessionService: SessionService
    ) {
        super(http, sessionService)
        this.isAuthenticated = < BehaviorSubject < boolean > > new BehaviorSubject(null)
        this.currentUser = < BehaviorSubject < User > > new BehaviorSubject(null)
    }

    subscribeToIsAuthenticated():Observable< boolean > {
        return this.isAuthenticated.asObservable()
    }

    login(username: string, password: string) {
        this.$localGet(`/users?username=${username}&password=${password}&_limit=1`)
        .pipe(map(mapToUsers))
        .subscribe(user => {
            if(user.length == 1) {
                let newUser = user[0]
                this.currentUser.next(newUser)
                this.isAuthenticated.next(true)
                this.sessionService.setSessionUserId(newUser.id)
            }
        })
    }

    fetchCurrentUser() {
        let userId = this.sessionService.getSessionUserId()
        console.log('userid', userId)
        if(userId) {
            this.$localGet(`/users/${userId}`)
            .pipe(map(mapToUser))
            .subscribe(user => {
                if(user) {
                    console.log('fetch user', user)
                    this.currentUser.next(user)
                    this.isAuthenticated.next(true)
        
                    this.sessionService.setSessionUserId(user.id)
                }
            })
        } else {
            this.sessionService.clearCookies()
            this.currentUser.next(null)
            this.isAuthenticated.next(false)
        }
    }

    isLoggedIn() {
        const changeToAuthTokenTempoUserId = this.sessionService.getSessionUserId()
        return changeToAuthTokenTempoUserId
    }

    logout() {
        this.isAuthenticated.next(false)
        this.currentUser.next(null)
        this.sessionService.clearCookies()
    }

}
function mapToUsers(response): User[] {
    return response.map(mapToUser)
}
function mapToUser(response): User {
    let user = new User
    user.id = response['id']
    user.username = response['username']
    user.email = response['email']
    user.mobile_number = response['mobile_number']

    return user
}