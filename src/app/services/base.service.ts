import { HttpClient, HttpHeaders, HttpHeaderResponse } from '@angular/common/http';
import { environment }   from "../../environments/environment";
import { SessionService } from './session.service';
import { ClrDatagridStateInterface } from '@clr/angular';
import { filter } from 'rxjs/operators';

export class BaseService {
    private localBaseUrl: string = 'http://0.0.0.0:3000';
    base_url = environment.baseUrl;
    withToken: boolean = true;
    constructor(public http: HttpClient, public sessionService: SessionService) {
    }


    public getHeaders() {
        const headers = new HttpHeaders().append('Content-Type', 'application/json');

        if(this.sessionService.getSessionAuthToken() && this.withToken) {

            const headers = new HttpHeaders().append('Content-Type', 'application/json')
                                           .append('Authorization', 'token ' + this.sessionService.getSessionAuthToken());
            return headers;
        }

        return headers;
    }

    public $get(url: string, withToken: boolean = true) {
        this.withToken = withToken;
        
        return this.http.get(this.base_url + url, { headers: this.getHeaders() });
    }

    public $post(url: string, data: any, withToken: boolean = true) {
        this.withToken = withToken;
        return this.http.post(this.base_url + url, JSON.stringify(data), {headers: this.getHeaders()});
    }

    public $put(url: string, data: any, withToken: boolean = true) {
      this.withToken = withToken;
        return this.http.put(this.base_url + url, JSON.stringify(data), {headers: this.getHeaders()});
    }

    public $delete(url: string, withToken: boolean = true) {
        this.withToken = withToken;
        return this.http.delete(this.base_url + url, {headers: this.getHeaders()});
    }

    public $localGet(url: string, state: ClrDatagridStateInterface = null) {
        let buildUrl = this.stateFilter(url, state)
        console.log(buildUrl)
        return this.http.get(this.localBaseUrl + buildUrl);
    }

    public $localPost(url: string, data: any, withToken: boolean = true) {
        this.withToken = withToken;
        return this.http.post(this.localBaseUrl + url, JSON.stringify(data), {headers: this.getHeaders()});
    }

    public $localPut(url: string, data: any, withToken: boolean = true) {
      this.withToken = withToken;
      return this.http.put(this.localBaseUrl + url, JSON.stringify(data), {headers: this.getHeaders()});
    }

    public sort(haystack: any [], fieldName: string, arrangementOrder: string) {
        const sortAsc = (arrangementOrder == "asc");
       
        let stack = haystack.sort((n1, n2) => {
          if (n1[fieldName] < n2[fieldName]) {
            return (sortAsc) ?  -1 : 1;
          } //sort string ascending
          if (n1[fieldName] > n2[fieldName]) {
            return (sortAsc) ? 1 : -1;
          }
          return 0;
        })
        
        return stack;
    }

    private stateFilter(url, state: ClrDatagridStateInterface) {
        let filterUrl = url
        
        let index = filterUrl.indexOf('?');
        if(state != null) {
            let filters:{[prop:string]: any[]} = {}
            if (state.filters) {
                for (let filter of state.filters) {
                    let {property, value} = <{property: string, value: string}>filter
                    filters[property] = [value]
                }
            }
            if(state.sort) {
                let order = state.sort.reverse == true ? 'asc' : 'desc'
                let  urlSeperator = this.getUrlParamsSeperator(filterUrl)
                filterUrl += `${urlSeperator}_sort=${state.sort.by}&_order=${order}`
            }
            if(state.filters) {
                state.filters.forEach(filter => {
                    let  urlSeperator = this.getUrlParamsSeperator(filterUrl)
                    filterUrl += `${urlSeperator}${filter['property']}_like=${filter['value']}`
                })
            }
        }    

        return filterUrl
    }

    private getUrlParamsSeperator(url) {
        return url.indexOf('?') >= 0 ? '&' : '?'
    }
}