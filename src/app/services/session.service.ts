import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  private sessionAuthToken: string = "";
  private sessionUserID: string = "";
  private loggedInByKey: string = "";

  constructor(
    private cookieService: CookieService
  ) {

  }

  setSessionAuthToken(auth_token) {
    this.sessionAuthToken = auth_token
    this.cookieService.set('auth_token', auth_token);
  }

  getSessionAuthToken() {
    if (this.cookieService.get('auth_token')) {
      return this.cookieService.get('auth_token');
    }
    else if (this.sessionAuthToken) {
      return this.sessionAuthToken;
    } else {
      return null;
    }

  }

  setSessionUserId (userId) {
    this.sessionUserID = userId;
    this.cookieService.set("uId", userId);
  }

  getSessionUserId() {
    if (this.cookieService.get('uId')) {
      return this.cookieService.get('uId');
    }
    else if (this.sessionUserID) {
      return this.sessionUserID;
    } else {
      return null;
    }

  }

  setLoggedInByKey(condition) {
    this.loggedInByKey = condition;
    this.cookieService.set("loggedInByKey", condition);
  }

  clearCookies() {
    this.cookieService.deleteAll();
  }
 
}