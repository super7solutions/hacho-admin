import { UserService } from "./user.service"
import { HttpClient, HttpClientModule } from "@angular/common/http"
import { TestBed } from "@angular/core/testing"
import { CookieService } from "ngx-cookie-service"
import { User } from '../model/user.model'

describe('UserService', () => {
    let service: UserService

    beforeEach(() => { 
        TestBed.configureTestingModule({
            imports: [
                HttpClientModule
            ],
            providers: [
                CookieService
            ]
        });
        this.service = TestBed.get(UserService)
    })

    it('should create an instance', () => {
        expect(this.service).toBeDefined()
    })

    // it('should return users from observable', (done: DoneFn) => {
    //     this.service.subscribeToUsers().subscribe(users => {
    //         expect(users instanceof User[]).toBe(true, 'instance of User[]')
    //         done()
    //     })

    //     this.service.fetchUsers()
    // })




})