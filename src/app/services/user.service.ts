import { Injectable } from '@angular/core'
import { BaseService } from './base.service'
import { HttpClient } from '@angular/common/http'
import { SessionService } from './session.service'
import { BehaviorSubject, Observable } from 'rxjs'
import { User } from '../model/user.model'
import { map } from 'rxjs/operators'
import { Activity } from '../model/activity.model'
import { ClrDatagridStateInterface } from '@clr/angular'
import { LevelUser } from '../model/level.user.model';

@Injectable({
    providedIn: 'root'
})
export class UserService extends BaseService {
    users: BehaviorSubject < User[] >
    levelUserList: BehaviorSubject < LevelUser [] >
    activities: BehaviorSubject < Activity [] >
    // usersIsLoading: BehaviorSubject < boolean > 
    constructor(
        http: HttpClient,
        sessionService: SessionService
    ) {
       super(http,sessionService)
       this.users = < BehaviorSubject < User[] > > new BehaviorSubject([])
       this.levelUserList = < BehaviorSubject < LevelUser [] > > new BehaviorSubject([])
       this.activities = < BehaviorSubject < Activity [] > > new BehaviorSubject([])
    //    this.usersIsLoading = < BehaviorSubject < boolean > > new BehaviorSubject(null)
    }

    fetchUsers(state: ClrDatagridStateInterface = null) {
        // this.setUserLoading()
        this.$localGet('/users?_expand=levelUser', state)
        .pipe(map(mapToUsers))
        .subscribe(users => {
            this.users.next(users)
            // this.setUserLoading(false)
        })
    }

    fetchActivities(userId: number) {
        this.$localGet(`/activities?userId=${userId}&_sort=createdAt&_order=asc`)
        .pipe(map(mapToActivities))
        .subscribe(activities => {
            this.activities.next(activities)
        })
    }

    fetchLevelUser(state: ClrDatagridStateInterface = null) {
        this.$localGet('/levelUsers', state)
        .pipe(map(mapToLevelUsers))
        .subscribe(result => {
            this.levelUserList.next(result)
        })
    }

    clearActivities() {
        this.activities.next([])
    }
    destroyUsers() {
        this.users.next([])
        // this.usersIsLoading.next(null)
    }

    clearLevelUsers() {
        this.levelUserList.next([])
    }

    subscribeToUsers():Observable<User []> {
        return this.users.asObservable()
    }

    subscribeToActivities(): Observable < Activity [] > {
        return this.activities.asObservable()
    }

    subscribeToLevelUserList(): Observable < LevelUser [] > {
        return this.levelUserList.asObservable()
    }
    // subscribeToUsersLoading():Observable< boolean > {
    //     return this.usersIsLoading.asObservable()
    // }

    // setUserLoading(loading: boolean = true) {
    //     setTimeout(() => {
    //         this.usersIsLoading.next(loading)
    //     }, 50)
    // }

    getUserById(id: number) {
        return this.$localGet(`/users/${id}?_expand=levelUser`).pipe(map(mapToUser))
    }

    updateUser(user: User) {
        return this.$localPut(`/users/${user.id}`, user)
    }

}

function mapToUsers(users): User[] {
    return users.map(mapToUser)
}
function mapToUser(user): User {
    let newUser = new User
    newUser.id = user['id']
    newUser.username = user['username']
    newUser.email = user['email']
    newUser.agency = user['agency']
    newUser.mobile_number = user['mobile_number']
    newUser.levelUser = mapToLevelUser(user['levelUser'])
    newUser.status = user['status']
    newUser.credits = user['credits']
    return newUser
}

function mapToActivities(activities): Activity [] {
    return activities.map(mapToActivity)
}

function mapToActivity(activity): Activity {
    let newActivity = new Activity
    newActivity.id = activity['id']
    newActivity.userId = activity['userId']
    newActivity.action = activity['action']
    newActivity.description = activity['description']
    newActivity.type = activity['type']
    newActivity.createdAt = activity['createdAt']
    
    return newActivity
}

function mapToLevelUsers(levelUsers):LevelUser[] {
    return levelUsers.map(mapToLevelUser)
}

function mapToLevelUser(levelUser): LevelUser {
    let levelUserObject = new LevelUser
    levelUserObject.id = levelUser['id']
    levelUserObject.name = levelUser['name']
    levelUserObject.depositTotalAmount = levelUser['depositTotalAmount']
    levelUserObject.remarks = levelUser['remarks']
    levelUserObject.status = levelUser['status']
    levelUserObject.totalBetAmount = levelUser['totalBetAmount']
    levelUserObject.updatedAt = levelUser['updatedAt']
    levelUserObject.createdAt = levelUser['createdAt']
    

    return levelUserObject
}