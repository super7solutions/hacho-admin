import { AuthService } from '../services/auth.service';
import { Directive, HostListener, ElementRef } from "@angular/core";

@Directive({
  selector: '[hasNoPermission]',
  host: {
    
  }
})
export class HasNoPermissionDirective { 
  authenticated: boolean;
  constructor(
    private authService: AuthService,
    private element: ElementRef
  ) {

    this.authService.subscribeToIsAuthenticated().subscribe((authenticated) => {
        if(authenticated) {
            this.element.nativeElement.style.display = 'none';
        }
        else {
            this.element.nativeElement.style.display = 'block';
        }
    })
  }


  
}