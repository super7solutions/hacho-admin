import { AuthService } from '../services/auth.service';
import { Directive, HostListener, ElementRef } from "@angular/core";

@Directive({
  selector: '[hasPermission]',
  host: {
    
  }
})
export class HasPermissionDirective { 
  authenticated: boolean;
  constructor(
    private authService: AuthService,
    private element: ElementRef
  ) {
    this.authenticated = false;
    this.authService.subscribeToIsAuthenticated().subscribe((authenticated) => {
      
      this.authenticated = authenticated;
      this.renderElement();
    })
  }

  ngOnInit() {
    this.renderElement();
  }
  
  private renderElement () {
    if (this.authenticated) {
      this.element.nativeElement.style.display = 'block';
    }
    else {
      this.element.nativeElement.style.display = 'none';
    }
  }

  
}