import { Component, OnInit, Input, EventEmitter, Output, OnChanges } from '@angular/core';
import { DepositTransaction } from 'src/app/model/deposit-transaction.model';

@Component({
    selector: 'app-audit-deposit-modal',
    templateUrl: './audit-deposit.modal.component.html',
    styleUrls: ['./audit-deposit.modal.component.scss']
})
export class AuthDepositModalComponent implements OnInit,OnChanges {
    @Input('displayModal') displayModal : boolean
    @Input('deposit') deposit: DepositTransaction
    @Output() closeModal : EventEmitter< string >  = new EventEmitter
    @Output() submit: EventEmitter < DepositTransaction > = new EventEmitter
    constructor() { }

    ngOnInit(): void { }

    ngOnChanges(): void {
        console.log('this', this.deposit)
    }
    close() {
        this.closeModal.emit('audit')
    }

    setStatus(status: string) {
        let deposit = this.deposit
        deposit.status = status
        this.save(deposit)
    }

    private save(deposit: DepositTransaction) {
        this.submit.emit(deposit)
        this.close()
    }
}
