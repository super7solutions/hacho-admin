import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-date',
    template: `{{ date | date: "y-MM-dd HH:mm" }}`
})
export class DateComponent implements OnInit {
    @Input('date') date;
    constructor() { }

    ngOnInit() { }
}