import { Component, OnInit, Input, OnChanges, OnDestroy, Output, EventEmitter } from '@angular/core';
import { DepositTransaction } from 'src/app/model/deposit-transaction.model';

@Component({
    selector: 'app-view-deposit-modal',
    templateUrl: './view-deposit.modal.component.html',
    styleUrls: ['./view-deposit.modal.component.scss']
})
export class ViewDepositModalComponent implements OnInit,OnChanges,OnDestroy {
    @Input('displayModal') displayModal: boolean
    @Input('deposit') deposit: DepositTransaction
    @Output() closeModal: EventEmitter< string > = new EventEmitter()
    constructor(
    ) { }

    ngOnInit(): void { }

    ngOnDestroy(): void {
        this.deposit = null;
    }

    close() {
        this.closeModal.emit('view')
    }

    ngOnChanges():void {
        console.log('deposit', this.deposit)
    }
}
