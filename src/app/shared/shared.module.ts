import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { ClarityModule } from '@clr/angular';
import { HasPermissionDirective } from '../directives/has-permission.directive';
import { HasNoPermissionDirective } from '../directives/has-no-permission.directive';
import { DateComponent } from './date/date.component';
import { ViewDepositModalComponent } from './view-deposit/view-deposit.modal.component';
import { AuthDepositModalComponent } from './audit-deposit/audit-deposit.modal.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ViewWithdrawModalComponent } from './view-withdraw/view-withdraw.modal.component';
@NgModule({
    declarations: [
        HeaderComponent,
        HasPermissionDirective,
        HasNoPermissionDirective,
        DateComponent,
        ViewDepositModalComponent,
        AuthDepositModalComponent,
        ViewWithdrawModalComponent
    ],
    imports: [ 
        CommonModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        ClarityModule
     ],
    exports: [
        HeaderComponent,
        DateComponent,
        ViewDepositModalComponent,
        AuthDepositModalComponent,
        ViewWithdrawModalComponent
    ],
    providers: [],
})
export class SharedModule {}