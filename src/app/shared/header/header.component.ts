import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { SessionService } from 'src/app/services/session.service';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    public appName: string = "Hacho Admin"
    public currentLanguage: string;
    constructor(
        private translateService: TranslateService,
        private authService: AuthService,
        private router: Router
    ) { 
        this.currentLanguage = this.translateService.getDefaultLang();
    }

    ngOnInit(): void {
        // this.currentLanguage = this.translateService.language();
     }

    selectLanguage(language) {
        this.translateService.use(language);
        this.currentLanguage = language;
    }
}
