import { Component, OnInit, Output, Input, EventEmitter, OnDestroy, OnChanges } from '@angular/core';
import { WithdrawTransaction } from '../../model/withdraw-transaction.model';

@Component({
    selector: 'app-view-withdraw-modal',
    templateUrl: './view-withdraw.modal.component.html',
    styleUrls: ['./view-withdraw.modal.component.scss']
})
export class ViewWithdrawModalComponent implements OnInit,OnDestroy,OnChanges {
    @Input('displayModal') displayModal: boolean
    @Input('withdraw') withdraw: WithdrawTransaction
    @Output() closeModal: EventEmitter< string > = new EventEmitter()
    constructor() { }

    ngOnInit(): void { }

    ngOnDestroy(): void {
        this.withdraw = null
    }

    ngOnChanges(): void {
        console.log(this.withdraw)
    }

    close() {
        this.closeModal.emit()
    }


}
