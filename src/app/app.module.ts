import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'

import {HttpClientModule, HttpClient} from '@angular/common/http'
import { AppComponent } from './app.component'
import { ClarityModule } from '@clr/angular'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { SharedModule } from './shared/shared.module'
import { AppRoutingModule } from './app-routing.module'
import { SimpleLayoutComponent } from './layouts/simple/simple.layout.component'
import { FullLayoutComponent } from './layouts/full/full.layout.component'
import {
  TranslateModule,
  TranslateLoader,
  TranslatePipe
} from "@ngx-translate/core"
import {TranslateHttpLoader}            from '@ngx-translate/http-loader'
import { FormsModule } from '@angular/forms'
import { CommonModule } from '@angular/common'
import { CookieService } from 'ngx-cookie-service'
import { NgxChartsModule } from '@swimlane/ngx-charts'
import { OrderModule } from 'ngx-order-pipe'
import { RouterModule } from '@angular/router';
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, '/assets/i18n/', '.json')
}

@NgModule({
  declarations: [
    AppComponent,
    SimpleLayoutComponent,
    FullLayoutComponent
  ],
  imports: [
    BrowserModule,
    ClarityModule,
    BrowserAnimationsModule,
    SharedModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    CommonModule,
    NgxChartsModule,
    OrderModule,
    RouterModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: (createTranslateLoader),
          deps: [HttpClient]
      }
    }),
  ],
  providers: [CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
