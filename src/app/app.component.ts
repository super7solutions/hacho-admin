import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
	title = 'HachoAdmin';

	constructor(
		translateService: TranslateService,
		private authService: AuthService
	) {
		translateService.setDefaultLang('zh-cn');
	}

	ngOnInit() {
		this.authService.fetchCurrentUser();
	}
}
